const QrCode = require("qrcode-reader");
const Jimp = require("jimp");
const fs = require("fs");


module.exports = function(imgPath) {
    var buffer = fs.readFileSync(imgPath);
    return new Promise(function(res, rej) {
        Jimp.read(buffer, function(err, image) {
            if (err) {
                rej(err);
            }
            var qr = new QrCode();
            qr.callback = function(err, value) {
                if (err) {
                    rej(err);
                }
                res(value.result);
            };
            qr.decode(image.bitmap);
        });
    });
};
