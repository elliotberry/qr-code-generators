const download = require('image-downloader')

var fs = require('fs');
var dir = '/tmp/qr/';



module.exports = function (url) {
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
    return new Promise(function (res, rej) {
        const options = {
            url: url,
            dest: dir + '1.png'                // Save to /path/to/dest/image.jpg
        }
        download.image(options)
            .then(({ filename, image }) => {
                console.log('Saved to', filename)  // Saved to /path/to/dest/image.jpg
            })
            .catch((err) => console.error(err))

    });
};
