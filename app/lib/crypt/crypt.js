let enc = require("./enc.js");
let dec = require("./dec.js");




module.exports = {
    decrypt: dec,
    encrypt: enc
};