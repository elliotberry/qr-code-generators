const crypto = require('crypto');
module.exports = {
    getCipherKey: function (password) {
        return crypto.createHash('sha256').update(password).digest();
    }
}