var imgur = require('imgur');
//Setting
imgur.setAPIUrl('https://api.imgur.com/3/');

module.exports = function(bsixtyfour) {
    return new Promise(function (res, rej) {
    imgur.uploadBase64(bsixtyfour)
        .then(function (json) {
           res(json.data.link);
        })
        .catch(function (err) {
            console.error(err.message);
        });

    });
};
