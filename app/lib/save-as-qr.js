var QRCode = require('qrcode');
var toSJIS = require('qrcode/helper/to-sjis');
var fs = require('fs');

async function toCodes(data) {
    var chunks = [];

    //Splitting up if larger than i qrcode
    for (var i = 0, charsLength = data.length; i < charsLength; i += 200) {
        chunks.push(data.substring(i, i + 3));
    }
    console.log("File is " + chunks.length + " QRs long.")
   let y = await doo(chunks);
   return y;
}


async function doo(arr, type = "url") {
    return new Promise(function(res, rej) {
    if (type == "url") {
        for (o = 0; o < arr.length; o++) {
            QRCode.toDataURL(arr[o], {
                errorCorrectionLevel: 'H'
            }, function (err, url) {
                res(url);
            })
        }
    }
});

}

module.exports = toCodes;