HEADSIZE=$(stat -f%z $BASETEMP/compress.log)
COMPSIZE=$(stat -f%z $BASETEMP/test.7z)
FILENAME=$1
FILESIZE=$(stat -f%z "$FILENAME")
APPROXSIZE=$FILESIZE * 3.5
COMPRATIO=`bc -l <<< "($COMPSIZE / $HEADSIZE) * 100"`
COMPRATIO=`printf '%.0f\n' $COMPRATIO`
QRNUMBER=`bc -l <<< "($FILESIZE * ($COMPSIZE / $HEADSIZE)) / $QRSIZE"`
QRNUMBER=`printf '%.0f\n' $QRNUMBER`
echo "Approximately $COMPRATIO% compression ratio calculated. $QRNUMBER QR codes will be generated."
echo "A $(($QRNUMBER / $FRAMERATE)) second video will be generated."