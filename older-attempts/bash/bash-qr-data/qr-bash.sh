#!/bin/bash
$include ./qr.conf
BASE=`pwd`
$include ./stats.sh
# delete temp files
#rm -rf $BASETEMP/*/*

#sample compression for stats
head $1 > $BASETEMP/compress.log
7z a -mx=5 $BASETEMP/test.7z $BASETEMP/compress.log > /dev/null

echo "Is this OK(y/N)?"
read prompt

if [[ "$prompt" != "y" ]]; then
    echo "Later, gator."
fi
if [[ "$prompt" = "y" ]]; then
    #zip up the file based on predetermined size.
    7z a -mx=5 -v$QRSIZEb $ZIPDIR/files.7z $1


    i=0
    for filename in $ZIPDIR/*; do
        i=$((i+1))
        echo "Completed $i / $QRNUMBER QR codes"
        name=${filename##*/}
        base=${name%.txt}
        qrencode -r "$filename" -l h -o $QRDIR/"$base".png
    done
    #output video
    cat ./qr-output/* | ffmpeg -f image2pipe -r 15 -i - -pix_fmt yuv420p -s 1920x1080 -crf 25 -vcodec libx264 ./out.mp4
    rm -rf $BASETEMP
fi