#!/bin/bash
#includes, base vars


source ./inc/size-stats.sh #gets stsatistics on file size and compression in order to guess how long this will take

echo "Is this OK(y/N)?" #Prompt the output video/files size
read prompt
if [[ "$prompt" != "y" ]]; then
    echo "Later, gator."
fi
if [[ "$prompt" = "y" ]]; then
    ./std.sh | ffmpeg -f image2pipe -r 5 -i - -pix_fmt yuv420p -s 720x480 -crf 25 -vcodec libx264 ./$1.mp4
fi