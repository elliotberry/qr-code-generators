#!/bin/bash

if [ ! -f $1 ]; then
    echo "File not found!"
fi

FILESIZE=$(stat -f%z "$1")
QRCOUNT=`bc -l <<< "($FILESIZE / 1000)"`
QRNUMBER=$(echo $QRCOUNT | awk '{printf("%d\n",$1 + 0.5)}')
SECONDS="$(($QRNUMBER / 10))"
MINUTES="$(($SECONDS / 60))"
HOURS="$(($MINUTES / 60))"
echo "File weighs $FILESIZE bytes. Number of QR codes generated will be $QRNUMBER. At 10 frames a second (600K/m, 36M/hr), this video will last $HOURS hours, or $SECONDS seconds."