#sample compression for stats
head $1 > $BASETEMP/compress.log #Get head data of file, compress it as test
7z a -mx=5 $BASETEMP/test.7z $BASETEMP/compress.log > /dev/null

#tests compression to get the approximate output size of file
HEADSIZE=$(stat -f%z $BASETEMP/compress.log)
COMPSIZE=$(stat -f%z $BASETEMP/test.7z)
FILENAME=$1
FILESIZE=$(stat -f%z "$FILENAME")
APPROXSIZE=$FILESIZE * 3.5
COMPRATIO=`bc -l <<< "($COMPSIZE / $HEADSIZE) * 100"`
COMPRATIO=`printf '%.0f\n' $COMPRATIO`
QRNUMBER=`bc -l <<< "($FILESIZE * ($COMPSIZE / $HEADSIZE)) / $QRSIZE"`
QRNUMBER=`printf '%.0f\n' $QRNUMBER`
echo "Approximately $COMPRATIO% compression ratio calculated. $QRNUMBER QR codes will be generated."
echo "A $(($QRNUMBER / $FRAMERATE)) second video will be generated."