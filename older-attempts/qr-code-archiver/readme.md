#Encoding files as video
Encodes a file as a series of QR codes, and then embeds those QR codes into a video file. Here's the statistics for QR data:

    Numeric only    Max. 7,089 characters (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
    Alphanumeric    Max. 4,296 characters (0–9, A–Z [upper-case only], space, $, %, *, +, -, ., /, :)
    Binary/byte     Max. 2,953 characters (8-bit bytes) (23624 bits)
    Kanji/Kana  Max. 1,817 characters

At alphanumeric and 25fps, we can do approx. 4k x 25, or 100k a second, to be conserative. This puts us at 6mb per minute.

1GB would take 166.6 minutes of footage. Footage takes 90000 QR codes per hour.